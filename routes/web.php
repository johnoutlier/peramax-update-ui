<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/test',function(){
	return view('test');
});

Route::get('/dashboard',function(){
    return view('dashboard');
});

Route::get('/userValidate',function(){
    return view('userValidate');
});

Route::get('/createTransaction', function(){
    return view('createTransaction');
});

Route::get('/transactionList', function(){
    return view('transactionList');
});

Route::get('/branchPayment', function(){
    return view('branchPayment');
});

Route::get('/holdbranch', function(){
    return view('holdbranch');
});

Route::get('/generalreport', function(){
    return view('generalReport');
});

Route::get('/austrac', function(){
    return view('austracReport');
});

Route::get('/counts', function(){
    return view('counts');
});

Route::get('/certificate', function(){
    return view('certificate');
});

Route::get('/banktobank', function(){
    return view('banktobank');
});

Route::get('/usersList', function(){
    return view('usersList');
});

Route::get('/listofsenders', function(){
    return view('listOfSenders');
});

Route::get('/branches', function(){
    return view('branches');
});

Route::get('/modelist', function(){
    return view('modeList');
});

Route::get('/servicecenter', function(){
    return view('serviceCenter');
});

Route::get('/exchangerateall', function(){
    return view('exchangeRateAll');
});