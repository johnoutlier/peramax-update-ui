<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/showExchangerate', 'ExchangeRatesController@getLast');


Route::get('serviceMode/searchMode/{keyword}', 'App\Http\Controllers\ServiceModeController@search');
Route::post('exchangeRate/add', 'App\Http\Controllers\ExchangeRatesController@store');
Route::resource('serviceMode', 'ServiceModeController');

Route::resource('status', 'TransactionStatusController',['only' => ['index']]);

Route::get('allUsers', 'UserController@index');

Route::get('validate', 'ValidateController@index');

Route::get('countPerServiceMode', 'TransactionController@countPerServiceMode');

Route::get('allUsers', 'UserController@index');

Route::get('senders', 'SenderController@index');

Route::resource('agent', 'AgentController');

Route::resource('serviceCenter', 'ServiceCenterController');

Route::get('/exchangeRate', 'ExchangeRatesController@showAll');

Route::get('transactions/countPerServiceMode', 'TransactionController@countPerServiceMode');

Route::get('transactions/countPerAgent', 'TransactionController@countPerAgent');

// Route::get('transaction/report', 'TransactionController@findReport');

Route::get('transaction/logs', 'TransactionController@transactionLogs');
Route::get('transaction/chart', 'TransactionController@transactionChart');
Route::get('transaction/list', 'TransactionController@getListOfTransactions');
Route::get('transactions/findTransactionByGroup/{agent?}', 'TransactionController@findTransactionByGroup');


