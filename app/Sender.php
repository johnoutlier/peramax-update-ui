<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use DB;
use App\User;

class Sender extends Model
{
    protected $hidden = ['created_at','updated_at'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function sender_beneficiaries(){
        return $this->belongsToMany('App\Beneficiary','sender_beneficiaries','sender_id','beneficiary_id');
    }
    public function beneficiaries(){
        return $this->hasMany('App\Beneficiary');
    }
    public  function sender_identification(){
        return $this->hasMany('App\SenderIdentification');
    }

    public function agent_senders(){
        return $this->belongsToMany('App\Agent','agent_senders','sender_id','agent_id');
    }

    public function agent(){
        return $this->belongsToMany('App\Agent','agent_senders','sender_id','agent_id');
    }
    public static function count()
    {
        $volume = DB::table('transactions')
        ->select(DB::raw('DATE_FORMAT(created_at,"%b %Y") as created,COUNT(*) as total'))
            ->from(DB::raw('(SELECT sender_id, MIN(created_at) as created_at FROM transactions GROUP BY sender_id) AS t1 '))
            ->groupBy(DB::raw('DATE_FORMAT(created_at,"%b %Y")'))
            ->orderBy(DB::raw('DATE_FORMAT(created_at,"%Y"), DATE_FORMAT(created_at,"%m")'))
            ->get();
        return $volume;
    }
    public static function uploadSignature($base64){
        if ($base64){
            $data = $base64;
            $filename = 'img/uploads/' .uniqid() . 'signature.png';
            $source = fopen($data, 'r');
            $destination = fopen($filename, 'wb');
            stream_copy_to_stream($source, $destination);

            fclose($source);
            fclose($destination);
            return $filename;
        }

        return null;

    }

    public static function validate_user_beneficiary_service_mode($sender_id,$beneficiary_id,$service_mode_id){
        $count = Sender::from('senders as s')
                ->join('sender_beneficiaries AS sb','s.id','=','sb.sender_id')
                ->join('beneficiary_service_centers AS bsc','sb.beneficiary_id','=','bsc.beneficiary_id')
                ->where('s.id',$sender_id)
                ->where('sb.beneficiary_id',$beneficiary_id)
                ->where('bsc.id',$service_mode_id)
                //->get()
                ->count();
        if ($count >0){
            return true;
        }

       return false;
    }










}
