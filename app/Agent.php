<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $hidden = array('pivot');
    public function agent_senders(){
        return $this->belongsToMany('App\Agent','agent_senders','agent_id','sender_id');
    }
}
