<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;
use OwenIt\Auditing\AuditingTrait;
use Illuminate\Support\Facades\Auth;

class Transaction extends Model
{

    use AuditingTrait;
    protected $hidden = array('pivot');
    protected $fillable = ['sender_id','beneficiary_id','account_no'];
    protected $auditEnabled  = true;
    protected $dontKeepLogOf = ['created_at', 'updated_at'];
    protected $auditableTypes = ['created', 'saved', 'deleted'];
    public function user(){
        return $this->belongsTo('App\User');
    }
    public function agent(){
        return $this->belongsTo('App\Agent');
    }
        public static function volumePerMonth()
    {
        $volume = Transaction::select(DB::raw('DATE_FORMAT(created_at,"%b %Y") AS created, COUNT(*) AS total,
            SUM(total_payment) AS payment_total,
             SUM(charge_amount) AS charge_total,
             (SUM(total_payment) - SUM(charge_amount))  AS remit_total,
             ROUND(((SUM(total_payment) - SUM(charge_amount)) * ROUND(AVG(exchange_rate),2)),2) AS send_total,
             ROUND(AVG(exchange_rate),2)  AS rate_ave '))
            ->groupBy(DB::raw('DATE_FORMAT(created_at,"%b %Y")'))
            ->orderBy(DB::raw('DATE_FORMAT(created_at,"%Y"), DATE_FORMAT(created_at,"%m")'))
            ->get();
        return $volume;
    }
    public static function volumePerDay(){
        $volume = Transaction::
        whereRaw('created_at >= SUBDATE(now(),INTERVAL 28 DAY)')
            ->select(DB::raw('DATE_FORMAT(created_at,"%a, %b %d %Y") AS created, COUNT(*) AS total,
               SUM(total_payment) AS payment_total,
             SUM(charge_amount) AS charge_total,
             (SUM(total_payment) - SUM(charge_amount))  AS remit_total,
             ROUND(((SUM(total_payment) - SUM(charge_amount)) * ROUND(AVG(exchange_rate),2)),2) AS send_total,
             ROUND(AVG(exchange_rate),2)  AS rate_ave '))
            ->groupBy(DB::raw('DATE_FORMAT(created_at,"%a %b %d %Y")'))
            ->orderBy(DB::raw('DATE_FORMAT(created_at,"%Y"), DATE_FORMAT(created_at,"%m"),DATE_FORMAT(created_at,"%d")'))
            ->get();

        return $volume;

    }

    public static function countPerServiceMode(){
        $count = Transaction::from('transactions as t')
            ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
            ->join('mode_centers AS mc','mc.id','=','bsc.mode_center_id')
            ->join('service_modes AS sm','sm.id','=','mc.service_mode_id')
            ->whereRaw('t.created_at between ? AND ? ',[Carbon::today(),Carbon::today()->endOfDay()])
            ->select(DB::raw('t.created_at as created,sm.service_code, COUNT(*) AS total,
                SUM(total_payment) AS payment_total,
             SUM(charge_amount) AS charge_total,
             (SUM(total_payment) - SUM(charge_amount))  AS remit_total,
             ROUND(((SUM(total_payment) - SUM(charge_amount)) * ROUND(AVG(exchange_rate),2)),2) AS send_total,
             ROUND(AVG(exchange_rate),2)  AS rate_ave '))
            ->groupBy(DB::raw('mc.service_mode_id'))
            ->orderBy(DB::raw('beneficiary_service_center_id,total'))
            ->get();
        return $count;

    }
    public static function countPerAgent(){
        $count = Transaction::from('transactions as t')
            ->join('agents AS a','a.id','=','t.agent_id')
            ->whereRaw('t.created_at between ? AND ? ',[Carbon::today(),Carbon::today()->endOfDay()])
            ->select(DB::raw('t.created_at AS created,a.agent_code, COUNT(*) AS total,
                SUM(total_payment) AS payment_total,
             SUM(charge_amount) AS charge_total,
             (SUM(total_payment) - SUM(charge_amount))  AS remit_total,
            ROUND(((SUM(total_payment) - SUM(charge_amount)) * ROUND(AVG(exchange_rate),2)),2) AS send_total,
             ROUND(AVG(exchange_rate),2)  AS rate_ave '))
            ->groupBy(DB::raw('agent_id'))
            ->orderBy(DB::raw('agent_id,total'))
            ->get();
        return $count;

    }

    public static function findBy($wheres=null,$today=null,$from=null,$to=null,$status=null){
        $transaction = Transaction
            ::from('transactions AS t')
            ->join('agents AS a',DB::raw('a.id'),'=',DB::raw('t.agent_id'))
            ->join('senders AS s','s.id','=','t.sender_id')
            ->leftJoin('sender_identifications AS si','si.sender_id','=','s.id')
            ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
            ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
            ->join('mode_centers AS mc','mc.id','=','bsc.mode_center_id')
            ->join('service_modes AS sm','sm.id','=','mc.service_mode_id')
            ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
            ->join('transaction_statuses AS ts','ts.id','=','t.transaction_status_id')
            ->join('users as u','u.id','=','t.user_id')
            ->leftJoin('users as ur','ur.id','=','t.approved_by')
            ->leftJoin('users as us','us.id','=','t.completed_by')
            ->where(function ($query) use ($wheres,$today,$from,$to,$status){
                if ($wheres){
                    //$query->where($alias.".".$where,$equal);
                    foreach ($wheres as $value) {
                      
                            $query->where($value['alias'].$value['where'],$value['equal']);
                      
                        

                    }
                }
                if ($today){
                    if ($today == true){
                        $query->where('t.created_at','>=',Carbon::today());
                    }
                    
                }

                if ($from && $to){
                    $query->whereRaw('t.created_at between ? AND ?',[$from,Carbon::parse($to)->addDay()]);
                }
                if ($status){
                    $query->where('t.transaction_status_id',$status);
                }
            })
            ->where(function ($query){
                if (Auth::user()->roles[0]->role =='DirectClient'){
                    $query->where('t.sender_id',Auth::user()->sender->id);
                }
                if (Auth::user()->roles[0]->role =='Agent'){
                    $query->where('t.agent_id',Auth::user()->agent[0]->id);
                }
            })
            ->select('t.*','bsc.*','sm.*','sc.*','ts.status','si.*',
                't.id AS t_id',
                's.fname AS s_fname',
                's.mname AS s_mname',
                's.lname AS s_lname',
                's.gender AS s_gender',
                's.bday AS s_bday',
                's.place_of_birth AS s_place_of_birth',
                's.contact_no AS s_contact_no',
                's.citizenship AS s_citizenship',
                's.residency AS s_residency',
                's.country AS s_country',
                's.address AS s_address',
                's.address_ph AS s_address_ph',
                's.suburb AS s_suburb',
                's.state AS s_state',
                's.post_code AS s_post_code',
                's.occupation AS s_occupation',
                's.source_of_inc AS s_source_of_inc',
                's.email as s_email',

                'b.fname AS b_fname',
                'b.mname AS b_mname',
                'b.lname AS b_lname',
                'b.gender AS b_gender',
                'b.address AS b_address',
                'b.city AS b_city',
                'b.city AS b_province',
                'b.contact_no AS b_contact_no',
                'b.civil_status AS b_civi_status',
                'b.relationship AS b_relationship',
                'sc.id AS SC_id',
                'sm.id AS SM_id',
                't.created_at AS t_cdate',
                'a.agent_status AS a_agent_status',
                'a.business_name AS a_business_name',
                'a.agent_code AS a_agent_code',

                /*dean*/
                'u.fname AS u_fname',
                'u.mname AS u_mname',
                'u.lname AS u_lname',
                'ur.fname AS ur_fname',
                'ur.mname AS ur_mname',
                'ur.lname AS ur_lname',
                'us.fname AS us_fname',
                'us.mname AS us_mname',
                'us.lname AS us_lname'
            )
            ->groupBy('t.id')
            ->get();

        return $transaction;

    }
}
