<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceMode extends Model
{
    protected $hidden = ['created_at','updated_at'];
    
    public function service_center(){
        return $this->belongsToMany('App\ServiceCenter','mode_centers','service_mode_id','service_center_id');
    }
}
