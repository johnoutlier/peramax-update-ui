<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\AuditingTrait;
use JWTAuth;
class User extends Authenticatable
{
    use AuditingTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];
    /*public static $logCustomFields = [
        'last_access' => 'Last access on {elapsed_time}.',
        'publish_date'=>[
            'created' => 'Created date : new.last_access',
            'updated' => 'Updated date : from {old.last_access} new {new.last_access}}'
        ]
    ];*/
    protected $auditEnabled  = true;
    public static $logCustomFields = [
       'ip' => 'Registered from the address {ip|route|getAnotherthing}'
    ];
    public function logs()
    {
        return $this->hasMany(OwenIt\Auditing\Log::class, 'user_id');
    }



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public  function roles(){
        return $this->belongsToMany('App\Role','user_roles','user_id','role_id');
    }
    public  function sender(){
        return $this->hasOne('App\Sender');
    }

    public  function transaction(){
        return $this->hasMany('App\Transaction','user_id');
    }
    
    public function agent(){
        return $this->belongsToMany('App\Agent','user_roles','user_id','agent_id');
    }
    
    public function admin(){
        return $this->hasMany('App\Admin');
    }

    public function admin_agent(){
        return $this->belongsToMany('App\Agent','user_roles','user_id','agent_id');
    }

    public static function getPayload(){
        $payload = JWTAuth::parseToken()->getPayload();
        $role = $payload->get('role');
        $agent = $payload->get('agent');
        $user = $payload->get('user');
        return compact('role','agent','user');
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function socialProviders(){
        return $this->hasMany(SocialProvider::class);
    }



    
}
