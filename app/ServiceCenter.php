<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCenter extends Model
{
    //

    protected $hidden = ['created_at','updated_at'];
}
