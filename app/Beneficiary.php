<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiary extends Model
{
    //
    protected $hidden = ['created_at','updated_at'];
    public function sender(){
        return $this->belongsToMany('App\Sender');
    }
    public function service_modes(){
        return $this->belongsToMany('App\ServiceMode','beneficiary_service_centers','beneficiary_id','service_mode_id');
    }

    public function service_centers(){
        return $this->belongsToMany('App\ServiceCenter','beneficiary_service_centers','beneficiary_id','service_center_id');
    }
}
