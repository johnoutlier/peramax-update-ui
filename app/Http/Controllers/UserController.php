<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\users;
use App\UserRole;
use App\Role;
use DB;
use JWTAuth;


class UserController extends Controller

{

    public function index(){

        $userFilter = isset($_GET['search']) ? $_GET['search'] : "";

        $users = UserRole
                 ::from('user_roles')
                 ->join('users',DB::raw('users.id'),'=',DB::raw('user_roles.user_id'))
                 ->join('roles','roles.id','=','user_roles.role_id')
                 ->join('agents','agents.id','=','user_roles.agent_id')
                 ->select('users.*','roles.*','agents.*',
                     'users.email AS email',
                     'agents.email As agent_email',
                     'users.id AS id',
                     'agents.id AS agent_id',
                     'roles.id AS role_id');             
        
        if($userFilter){
            $users = $users->where('fname', 'like', '%'.$userFilter.'%')
                            ->orWhere('lname', 'like','%'.$userFilter.'%')
                            ->orWhere('mname', 'like','%'.$userFilter.'%')
                            ->orWhere('role', 'like','%'.$userFilter.'%')
                            ->orWhere('agent_code', 'like','%'.$userFilter.'%')
                            ->paginate(10);
        }
        else{
            $users = $users->paginate(10);
        }
     //  return response()->success(compact('users'));   
     return json_encode($users);

    }
    public function update(Request $request, $id)
    {        
        $user = User::where('id','=',$id)->firstOrFail();
        foreach ($request->except('token') as $key => $value){
            $user->$key = $value;
        }
        $user->save(); 
        return response()->success(compact('user'));
    }


     public function getLast(){

          $lastUser = User::all()->last();
        return response()->success(compact('lastUser'));
        
        
    }


    public function showRoles(){
            $roles = Role::all();
        return response()->success(compact('roles'));
    }


    public function store(Request $request)
    {
        //
        $user = new User;
        foreach ($request->all() as $key => $value) {
            $user->$key = $value;
        }
        $user->save();
        return response()->success(compact('user'));
    }


      public function addNewUser(Request $request){
        $this->validate($request, [
            'email'      => 'required|email|unique:users'
        ]);

        $user = new User;
        foreach ($request->all() as $key => $value) {
            $user->$key = $value;
        }
        $user->password = bcrypt($request->password);
        $user->save();
        return response()->success(compact('user'));
    }


    public function AddNewUserRole(Request $request){
        $userRole = new UserRole;
        foreach ($request->all() as $key => $value) {
            $userRole->$key = $value;
        }

        $userRole->save();
        return response()->success(compact('userRole'));
    }


    public function  test(Request $request){

        $user = User::find($request->id);
        $role = $user->roles()->get();
        //$sender = $user->sender()->get();
        switch ($role[0]->role){
            case 'admin':
                $admin = $user->admin()->get();
                $agent = $user->admin_agent()->get();
                break;
            case 'agent':
                $agent = $user->agent()->get();
            case 'direct_client':
                break;
        }

        return response()->success(compact('user','role','agent','admin'));
    }
    
    public function getRole(){
        $user = Auth::user();
        $role = $user->roles()->first();
        return response()->success(compact('role'));
    }
    
    public function getAgent(){
        $user = Auth::user();
        $agent = $user->admin_agent()->first();
        return response()->success(compact('agent'));
    }

    public function getUser(){
        $user = Auth::user();
        $role = $user->roles()->first();
        $agent = $user->admin_agent()->first();
        return response()->success(compact('user','role','agent'));
    }

    public function changePassword(Request $request){
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|min:8',
        ]);
        $credentials = $request->only('email', 'password');
        if (! $token = JWTAuth::attempt($credentials)) {
            return response()->error('Invalid credentials', 401);
        }
        $user = Auth::user();
        $user->password = bcrypt($request->newPassword);
        $user->save();
        return response()->success(compact($user));
    }

    public static function getPayload(Request $request){
        $payload = JWTAuth::parseToken()->getPayload();
        $role = $payload->get('role');
        $agent = $payload->get('agent');
        $user = $payload->get('user');
        return response()->success(compact('role','agent','user'));
    }

    public function user_sender(){
        $user = Auth::User();
        $user = User::findOrFail($user->id);
        $sender = $user->sender;
        return response()->success(compact('sender'));
    }

    public function user_beneficiary_service_center($beneficiary,$service_center){
        $user = Auth::User();
        $beneficiary = $user->sender->sender_beneficiaries->find($beneficiary);
        $services = DB::table('beneficiary_service_centers AS bsc')
        ->join('mode_centers as mc','mc.id','=','bsc.mode_center_id')
        ->join('service_modes AS sm','mc.service_mode_id','=','sm.id')
        ->join('service_centers AS sc','mc.service_center_id','=','sc.id')
        ->where('bsc.beneficiary_id',$beneficiary->id)
        ->where('bsc.id',$service_center)
        ->select('sm.*','sc.*','bsc.*','mc.id AS mc_id')
        ->first();    
        return response()->success(compact('beneficiary','services'));
    }

    public function me(){
        $user = Auth::User();
    }


     

  

}

