<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\Transaction;
use DB;
use Carbon\Carbon;
use Pusher\Laravel\PusherManager;
use Illuminate\Support\Collection;
use Auth;
use Mail;
use App\Agent;
use App;
use Validator;
use App\DeletedTransaction;
use App\Sender;
use App\Beneficiary;
//use Illuminate\Contracts\Validation\Validator;
class TransactionController extends Controller
{
    protected $pusher;
    /**
    * Initialize Pusher
    */
    public function __construct(PusherManager $pusher)
    {
        $this->pusher = $pusher;
        $this->user = Auth::user();
        $this->today = date('Y-m-d');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /* athan */

          $stransactions = transaction::
            where(function ($query){
                if (Auth::user()->roles[0]->role =='DirectClient'){
                    $query->where('transactions.sender_id',Auth::user()->sender->id);
                }
            })
            ->take(8)
            ->get();
     return response()->success(compact('stransactions'));     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    //Jonathan

    public function createTransactionNumber($agentMaxCount,$transaction_no){
        if($agentMaxCount < 9){
                return $transaction_no . '0' . ($agentMaxCount + 1);
        }
        else{
                return $transaction_no . ($agentMaxCount + 1);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sender_id' => 'required',
            'beneficiary_id' => 'required',
            'beneficiary_service_center_id' => 'required',
            'exchange_rate' => 'required',
            'send_amount' => 'required',
            'receive_amount' => 'required',
            'charge_amount' => 'required',
            'total_payment' => 'required'
        ]);

        $validate = Sender::validate_user_beneficiary_service_mode($request->sender_id,$request->beneficiary_id,$request->beneficiary_service_center_id);

        if (!$validate){
            return response()->error('Unable to Process this Transaction.Please verify yours Inputs!', 401);
        }
        $start_date = \Carbon\Carbon::now()->format('Y-m-d');
        $base_date = \Carbon\Carbon::now()->format('Y-m-d H:i:s');
        $agentMaxCount = Transaction::where('agent_id','=',$request->agent_id)->whereBetween('created_at',array($start_date,$base_date))->count();
        //$request->tracking_no = $request->tracking_no + $agentMaxCount +  1;

        /*while(Transaction::where('tracking_no',$request->tracking_no . $agentMaxCount)->count() > 0){
            $agentMaxCount =$agentMaxCount + 1;
        }*/
        //$request->offsetSet('tracking_no', $request->tracking_no . $agentMaxCount);
        //$request->tracking_no = $request->tracking_no . $agentMaxCount . 'TEST';

        $user = Auth::User();
        $trans = new Transaction;
        $inputs = $request->except('user_id','tracking_no','is_agent_transaction');
        if (Auth::user()->roles[0]->role =='DirectClient'){ // For security purpose
            $inputs['sender_id'] = Auth::user()->sender->id;
            $inputs['transaction_status_id'] = 1;
        }
        

        $agent= Auth::user()->admin_agent()->first();
        $agent_code = $agent->agent_code;
        $inputs['agent_id'] = $agent->id;
        if(isset($request->is_agent_transaction) && Auth::user()->roles[0]->role =='Admin' && $request->is_agent_transaction){
            $sender = Sender::find($request->sender_id);
            $agent = $sender->agent()->first();
            if(count($agent)){
                $agent_code = $agent->agent_code;
                $inputs['agent_id'] = $agent->id;
            }
            
        }
        $transaction_date = \Carbon\Carbon::now()->format('Ymd');
        $transaction_no = $agent_code . $transaction_date;
        foreach ($inputs as $key => $value){
            $trans->$key = $value;
        }
        $newtransaction = $user->transaction()->save($trans);

        $updateTrackingNo = Transaction::where('transactions.id','=',$newtransaction->id)
            ->update(['tracking_no' => $this->createTransactionNumber($agentMaxCount,$transaction_no)]);
        //$updateTrackingNo->update(['tracking_no' => $request->tracking_no . ($agentMaxCount + 1) . $newtransaction->id ]);
        $newtransaction->offsetSet('tracking_no', $this->createTransactionNumber($agentMaxCount,$transaction_no));

        $wheres = collect(array(["where" => "id","alias" => "t.","equal" => $newtransaction->id]));
        $toPush = Transaction::findBy($wheres)->first();
        $this->pusher->trigger('Admin', 'newTransaction',$toPush);
        return response()->success(compact('newtransaction','updateTrackingNo'));

    }
    /**
     * Display the specified resource.Search
     *
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction
                        ::from('transactions AS t')
                        ->join('agents AS a',DB::raw('a.id'),'=',DB::raw('t.agent_id'))
                        ->join('senders AS s','s.id','=','t.sender_id')
                        ->leftJoin('sender_identifications AS si','si.sender_id','=','s.id')
                        ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
                        ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
                        ->join('mode_centers as mc','mc.id','=','bsc.mode_center_id')
                        ->join('service_modes AS sm','sm.id','=','mc.service_mode_id')
                        ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
                        ->join('transaction_statuses AS ts','ts.id','=','t.transaction_status_id')
                        ->where('tracking_no','=',$id)
                        ->where(function ($query){
                            if (Auth::user()->roles[0]->role =='DirectClient'){
                                $query->where('t.sender_id',Auth::user()->sender->id);
                            }
                        })
                        ->select('t.*','bsc.*','sm.*','sc.*','si.*','ts.*',
                            's.fname AS s_fname',
                            's.mname AS s_mname',
                            's.lname AS s_lname',

                            's.gender AS s_gender',
                            's.bday AS s_bday',
                            's.place_of_birth AS s_place_of_birth',
                            's.contact_no AS s_contact_no',
                            's.citizenship AS s_citizenship',
                            's.residency AS s_residency',
                            's.country AS s_country',
                            's.address AS s_address',
                            's.address_ph AS s_address_ph',
                            's.suburb AS s_suburb',
                            's.state AS s_state',
                            's.post_code AS s_post_code',
                            's.occupation AS s_occupation',
                            's.source_of_inc AS s_source_of_inc',
                            's.email as s_email',

                            'b.fname AS b_fname',
                            'b.mname AS b_mname',
                            'b.lname AS b_lname',
                            'b.gender AS b_gender',
                            'b.address AS b_address',
                            'b.contact_no AS b_contact_no',
                            'b.civil_status AS b_civi_status',
                            'b.relationship AS b_relationship',
                            'a.business_name AS a_business_name',
                            'a.agent_code AS a_agent_code',
                            'ts.id AS ts_id',
                            'ts.status AS ts_status',
                            't.created_at as t_cdate'
                            
                            
                        )
                        ->first();

        return response()->success(compact('transaction'));


    }



     public function track($id)
    {
        $transaction = Transaction
                        ::from('transactions AS t')
                        ->join('transaction_statuses AS ts','ts.id','=','t.transaction_status_id')
                        ->where('tracking_no','=',$id)
                        ->select(
                            't.tracking_no',
                            'ts.id AS ts_id',
                            'ts.status AS ts_status'
                        )
                        ->first();
        return response()->success(compact('transaction'));


    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::
                     where('id','=',$id)
                     ->where('updated_at','=',$request->updated_at)
                     ->where(function ($query){
                         if (Auth::user()->roles[0]->role =='DirectClient'){
                             $query->where('transactions.sender_id',Auth::user()->sender->id);
                         }
                     })
                     ->firstOrFail();
        
        foreach ($request->except('token','updated_at','agent_id','user_id','is_agent_transaction') as $key => $value){
            $transaction->$key = $value;
        }
        
       
        if($request->transaction_status_id==1){

            $transaction->approved_by=0;
            $transaction->approved_at=null;

        }
        else if($request->transaction_status_id==2){
            $transaction->approved_by= Auth::user()->id;
            $transaction->approved_at=Carbon::now();
            
        }

        else if($request->transaction_status_id==3){
            $transaction->completed_by= Auth::user()->id;
            $transaction->completed_at=Carbon::now();

        }




        $transaction->updated_by = Auth::user()->id;
        $agent = Transaction::join('agents','transactions.agent_id','=','agents.id')
        ->join('user_roles','transactions.agent_id','=','user_roles.agent_id')
        ->join('senders','transactions.sender_id','=','senders.id')
        ->where('transactions.id','=',$id)
        ->select('agents.email AS agent_email',
                'agents.business_name AS agent_business_name',
                'user_roles.id AS role','senders.email AS sender_email',
                'senders.fname AS sender_fname','senders.lname AS sender_lname')
        
        ->first();
        $v_sender_email = Validator::make($agent->toArray(),[
            'sender_email' => 'email'
        ]);
        $v_agent_email = Validator::make($agent->toArray(),[
            'agent_email' => 'email'
        ]);
        if ($transaction->transaction_status_id === 3){
            if ($agent->role === 1 || $agent->role === 2){
                 if ($v_sender_email->fails()){
                    //return response()->error('Unable to Send Email: Invalid Sender Email, Please Update', 401);
                 }else{
    
                    if ($agent->sender_email){
                        $sender_name  = $agent->sender_fname . ' ' . $agent->sender_lname;
                        $this->sendMail($agent->sender_email,'Transaction Confirmation Status',$this->senderMail($sender_name,$transaction->tracking_no));
                    }
                 }
            }else{
                if ($v_agent_email->fails()){
                    //return response()->error('Unable to Send Email: Invalid Agent Email, Please Update', 401);
                }else{
                    if($agent->agent_email){
                        $this->sendMail($agent->agent_email,'Transaction Confirmation Status',$this->agentMail($agent->agent_business_name,$transaction->tracking_no));
                    }
                   
                }
            }

        }
        
        $transaction->save();
        return response()->success(compact('transaction'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if($user->allow_delete === 1){
            $transaction = Transaction::findOrFail($id);
            $delete_transaction = new DeletedTransaction;
            $to_delete_transaction =  $transaction;
            $to_delete_transaction->deleted_time = Carbon::now();
            $delete_transaction->insert($to_delete_transaction->toArray());
            $transaction->delete();

            return response()->success(compact('transaction'));
        }
        
        return response()->error('Unathorized to delete transactions');
        

    }

    public function findTran($trackingNo){

        $transaction = Transaction
            ::from('transactions AS t')
            ->join('agents AS a','a.id','=','t.agent_id')
            ->join('senders AS s','s.id','=','t.sender_id')
            ->leftJoin('sender_identifications AS si','si.sender_id','=','s.id')
            ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
            ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
            ->join('mode_centers as mc','mc.id','=','bsc.mode_center_id')
            ->join('service_modes AS sm','sm.id','=','mc.service_mode_id')
            ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
            ->where('t.tracking_no',$trackingNo)
            ->where(function ($query){
                if (Auth::user()->roles[0]->role =='DirectClient'){
                    $query->where('s.id',Auth::user()->sender->id);
                }
            })
            ->select('t.*','bsc.*','sm.*','sc.*','mc.*','si.*',
                's.fname AS s_fname',
                's.mname AS s_mname',
                's.lname AS s_lname',
                's.gender AS s_gender',
                's.bday AS s_bday',
                's.place_of_birth AS s_place_of_birth',
                's.contact_no AS s_contact_no',
                's.citizenship AS s_citizenship',
                's.residency AS s_residency',
                's.country AS s_country',
                's.address AS s_address',
                's.address_ph AS s_address_ph',
                's.suburb AS s_suburb',
                's.state AS s_state',
                's.post_code AS s_post_code',
                's.occupation AS s_occupation',
                's.source_of_inc AS s_source_of_inc',
                's.email as s_email',

                'b.fname AS b_fname',
                'b.mname AS b_mname',
                'b.lname AS b_lname',
                'b.gender AS b_gender',
                'b.address AS b_address',
                'b.contact_no AS b_contact_no',
                'b.civil_status AS b_civi_status',
                'b.relationship AS b_relationship',
                'a.business_name AS a_business_name',
                'a.agent_code AS a_agent_code'

            )
            ->first();

        return response()->success(compact('transaction'));
    }
    public function sendMail($to,$subject,$body){
        if ($to){
            Mail::send(array(),array(), function ($message) use ($to,$subject,$body) {
            $message->to($to, 'Test-Email')->subject($subject)
            ->setBody($body,'text/html');
            });
        }
     
    }

    public function agentMail($agent,$tracking_no){
       $body =  '<!DOCTYPE html>'
                .'<html>'
                .'<head>'
                . '<title>Sender</title>'
                .'</head>'
                .'<body style="font-size: 1.2emfont-family: "Sans Serif">'

                .'<br>'
                .'<div>'
                    . '<ul>'
                    .'<li style="list-style: none">Dear '. $agent .',</li>'
                    .'</ul>'    
                    .'<p style="margin-left: 10%">I would like to inform you that this transaction was completed Tracking no: '.$tracking_no .'.</p>'
                .'</div>'
                .'<br><br><br>'
                .'<div>'
                    .'<ul>'
                        .'<li style="list-style: none">Thanks,</li>'
                        .'<li style="list-style: none">Peramax Money Remittance Service</li>'
                    .'</ul>'
                .'</div>'

                .'</body>'
                .'</html>';
        return $body; 
    }

    public function senderMail($sender,$tracking_no){
        $body =  '<!DOCTYPE html>'
                . '<html>'
                . '<head>'
                    . '<title>Agent</title>'
                . '</head>'
                . '<body style="font-size: 1.2emfont-family: "Sans Serif">'
                .'<br>'
                . '<div>'
                    . '<ul>'
                        . '<li style="list-style: none">Dear '. $sender . ',</li>'
                    . '</ul>'   
                    . '<p style="margin-left: 10%">Thank you for trusting Peramax Money Remittance Service.</p>'
                    . '<p style="margin-left: 2.5%">We would like to inform you that your transaction was completed. Your Tracking no. is: '. $tracking_no . '.</p>'
                . '</div>'
                . '<br><br><br>'
                . '<div>'
                    . '<ul>'
                        . '<li style="list-style: none">Thanks,</li>'
                        . '<li style="list-style: none">Peramax Money Remittance Service</li>'
                    . '</ul>'
                . '</div>'

                . '</body>'
                . '</html>';
        return $body;   
    }


     /** ShowTransaction -athan */

     public function findlistReport(Request $request){
        //$request->between = array()
        $where = $request->where;
        $equal = $request->equal;
        $alias = $request->alias;
        $transactiontype = $request->transactiontype;


        $transaction = Transaction
            ::from('transactions AS t')
            ->join('agents AS a',DB::raw('a.id'),'=',DB::raw('t.agent_id'))
            ->join('senders AS s','s.id','=','t.sender_id')
            ->leftJoin('sender_identifications AS si','si.sender_id','=','s.id')
            ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
            ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
            ->join('mode_centers AS mc','mc.id','=','bsc.mode_center_id')
            ->join('service_modes AS sm','sm.id','=','mc.service_mode_id')
            ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
            ->join('transaction_statuses AS ts','ts.id','=','t.transaction_status_id')
            ->join('users as u','u.id','=','t.user_id')
                     ->where(function ($query) use ($alias,$where,$equal, $transactiontype){
                if ($alias && $where && $equal){
                    $query->where($alias.".".$where,$equal);
                }

                if($transactiontype) {
                    $query->where('t.transaction_type', $transactiontype);
                }
            })
            ->where(function ($query) use ($request){
                if ($request->from && $request->to){
                    $query->whereRaw('t.updated_at between ? AND ?',[$request->from,Carbon::parse($request->to)->addDay()]);
                }
                if(User::getPayload()['role']['role'] === 'DirectClient'){
                    $query->where('t.user_id',Auth::user()->id);
                }


            })
            ->where(function ($query) use ($request){
                if ($request->status){
                    $query->where('t.transaction_status_id',$request->status);
                }
            })
            ->where(function ($query){
                if (Auth::user()->roles[0]->role =='DirectClient'){
                    $query->where('t.sender_id',Auth::user()->sender->id);
                }
            })
            ->select('t.*','bsc.*','sm.*','sc.*','ts.status','si.*',
                't.id AS t_id',
                's.id AS sender_id',
                's.fname AS s_fname',
                's.mname AS s_mname',
                's.lname AS s_lname',
                's.gender AS s_gender',
                's.bday AS s_bday',
                's.place_of_birth AS s_place_of_birth',
                's.contact_no AS s_contact_no',
                's.citizenship AS s_citizenship',
                's.residency AS s_residency',
                's.country AS s_country',
                's.address AS s_address',
                's.address_ph AS s_address_ph',
                's.suburb AS s_suburb',
                's.state AS s_state',
                's.post_code AS s_post_code',
                's.occupation AS s_occupation',
                's.source_of_inc AS s_source_of_inc',
                's.email as s_email',

                'b.fname AS b_fname',
                'b.mname AS b_mname',
                'b.lname AS b_lname',
                'b.gender AS b_gender',
                'b.address AS b_address',
                'b.country AS b_country',
                'b.province AS b_province',
                'b.city AS b_city',
                'b.zip_code AS b_zip_code',
                'b.contact_no AS b_contact_no',
                'b.civil_status AS b_civi_status',
                'b.relationship AS b_relationship',
                'bsc.address AS bsc_address',
                'sc.id AS SC_id',
                'sm.id AS SM_id',

                    /*athan*/
                't.created_at AS t_cdate',
                't.updated_at AS t_updated_at',
                'a.agent_status AS a_agent_status',
                'a.business_name AS a_business_name',
                'a.agent_code AS a_agent_code',

                'u.fname AS u_fname',
                'u.mname AS u_mname',
                'u.lname AS u_lname'

            )
            ->groupBy('t.id')
            ->get();

        return response()->success(compact('transaction'));
    }



    //might replace
    public function findReport(Request $request){
        //$request->between = array()
        $where = $request->where;
        $equal = $request->equal;
        $alias = $request->alias;


        $transaction = Transaction
            ::from('transactions AS t')
            ->join('agents AS a',DB::raw('a.id'),'=',DB::raw('t.agent_id'))
            ->join('senders AS s','s.id','=','t.sender_id')
            ->leftJoin('sender_identifications AS si','si.sender_id','=','s.id')
            ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
            ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
            ->join('mode_centers AS mc','mc.id','=','bsc.mode_center_id')
            ->join('service_modes AS sm','sm.id','=','mc.service_mode_id')
            ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
            ->join('transaction_statuses AS ts','ts.id','=','t.transaction_status_id')
            ->join('users as u','u.id','=','t.user_id')
            ->where(function ($query) use ($alias,$where,$equal){
                if ($alias && $where && $equal){
                    $query->where($alias.".".$where,$equal);
                }
            })
            ->where(function ($query) use ($request){
                if ($request->from && $request->to){
                    $query->whereRaw('t.created_at between ? AND ?',[$request->from,Carbon::parse($request->to)->addDay()]);
                }
                if(User::getPayload()['role']['role'] === 'DirectClient'){
                    $query->where('t.user_id',Auth::user()->id);
                }


            })
            ->where(function ($query) use ($request){
                if ($request->status){
                    $query->where('t.transaction_status_id',$request->status);
                }
            })


            ->select('t.*','bsc.*','sm.*','sc.*','ts.status','si.*',
                't.id AS t_id',
                's.id AS sender_id',
                's.fname AS s_fname',
                's.mname AS s_mname',
                's.lname AS s_lname',
                's.gender AS s_gender',
                's.bday AS s_bday',
                's.place_of_birth AS s_place_of_birth',
                's.contact_no AS s_contact_no',
                's.citizenship AS s_citizenship',
                's.residency AS s_residency',
                's.country AS s_country',
                's.address AS s_address',
                's.address_ph AS s_address_ph',
                's.suburb AS s_suburb',
                's.state AS s_state',
                's.post_code AS s_post_code',
                's.occupation AS s_occupation',
                's.source_of_inc AS s_source_of_inc',
                's.email as s_email',

                'b.fname AS b_fname',
                'b.mname AS b_mname',
                'b.lname AS b_lname',
                'b.gender AS b_gender',
                'b.address AS b_address',
                'b.country AS b_country',
                'b.province AS b_province',
                'b.city AS b_city',
                'b.zip_code AS b_zip_code',
                'b.contact_no AS b_contact_no',
                'b.civil_status AS b_civi_status',
                'b.relationship AS b_relationship',
                'bsc.address AS bsc_address',
                'sc.id AS SC_id',
                'sm.id AS SM_id',

                    /*athan*/
                't.created_at AS t_cdate',
                't.updated_at AS t_updated_at',
                'a.agent_status AS a_agent_status',
                'a.business_name AS a_business_name',
                'a.agent_code AS a_agent_code',

                'u.fname AS u_fname',
                'u.mname AS u_mname',
                'u.lname AS u_lname'

            )
            ->groupBy('t.id')
            ->get();

        // return response()->success(compact('transaction'));
        return json_encode($transaction);
    }
    //Upgrade FindBy For Notification
    public function findBy(Request $request){
        $transaction = Transaction::findBy($request->wheres,$request->today,$request->from,$request->to,$request->status);
        return response()->success(compact('transaction'));
    }


    /**
     * @param null $agent
     * @param Request $request
     * @return mixed
     */
    public function findSenderTransactionByGroup(Request $request,$agent=null){
        $filters = $request->except('from','sender_id','blname');
        $filters2 = $request->except('from','sender_id','lname');
        
        $transaction = Transaction
            ::from('transactions AS t')
            ->join('agents AS a',DB::raw('a.id'),'=',DB::raw('t.agent_id'))
            ->join('senders AS s','s.id','=','t.sender_id')
            ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
            ->where(function ($query) use ($agent){
                if ($agent){
                    $query->where('t.agent_id',$agent);
                }
            })
            ->where(function ($query) use ($request){
                if ($request->sender_id){
                    $query->where('t.sender_id',$request->sender_id);
                }
            })
            ->where(function ($query) use ($filters){
                if ($filters){
                    foreach ($filters as $where =>$value){
                        $query->where('s.'.$where,'like', "%$value%");
                    
                    }
                }

            })

             ->where(function ($query) use ($filters2){
                if ($filters2){
                    foreach ($filters2 as $where =>$value){
                        $query->where('b.lname','like', "%$value%");
                    }
                }

            })

            ->where(function ($query) use ($request){
                if($request->from){
                    $query->where('t.created_at','>=',Carbon::parse($request->from)->addDay());
                }
            })
            ->select('t.tracking_no','t.created_at','t.id AS t_id',
                's.id AS s_id',
                's.fname AS s_fname',
                's.mname AS s_mname',
                's.lname AS s_lname',
                'b.fname AS b_fname',
                'b.mname AS b_mname',
                'b.lname AS b_lname',
                's.address AS s_address',
                's.suburb AS s_suburb',
                's.state AS s_state',
                'a.id AS a_id',
                DB::raw('COUNT(*) as count')
            )
            ->groupBy('s.id')
            ->get();

        return response()->success(compact('transaction'));
    }
    public function findSenderTransaction(Request $request,$sender,$agent=null){
        $filters = $request->except('from','sender_id');
        $transaction = Transaction
            ::from('transactions AS t')
            ->join('agents AS a',DB::raw('a.id'),'=',DB::raw('t.agent_id'))
            ->join('senders AS s','s.id','=','t.sender_id')
            ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
            ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
            ->join('mode_centers AS mc','mc.id','=','bsc.mode_center_id')
            ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
            ->where(function ($query) use ($agent){
                if ($agent){
                    $query->where('t.agent_id',$agent);
                }
            })
            ->where(function ($query) use ($sender){
                if ($sender){
                    $query->where('t.sender_id',$sender);
                }
            })
            ->where(function ($query) use ($filters){
                if ($filters){
                    foreach ($filters as $where =>$value){
                        $query->where('s.'.$where,'like', "%$value%");
                    }
                }

            })
            ->where(function ($query) use ($request){
                if($request->from){
                    $query->where('t.created_at','>=',Carbon::parse($request->from)->addDay());
                }
            })
            ->select(
                't.tracking_no',
                't.created_at',
                't.id AS t_id',
                't.receive_amount',
                't.send_amount',
                's.id AS s_id',
                's.fname AS s_fname',
                's.mname AS s_mname',
                's.lname AS s_lname',
                's.gender AS s_gender',
                's.address AS s_address',
                's.suburb AS s_suburb',
                's.state AS s_state',
                'b.fname AS b_fname',
                'b.mname AS b_mname',
                'b.lname AS b_lname',
                'sc.service_center',
                'a.id AS a_id'
            )
            ->groupBy('t.id')
            ->get();

        return response()->success(compact('transaction'));
    }
    public function findTransactionByGroup($agent=null){
        $transaction = Transaction::
            join('agents','agents.id','=','transactions.agent_id')
            ->where(function ($query) use ($agent){
                if($agent){
                    $query->where('agents.id','=',$agent);
                }
            })
            ->select('agent_id','business_name',DB::raw('TRUNCATE(SUM(total_payment) - (SUM(charge_amount)/2),2) AS TotalPayable,
                    (SELECT IFNULL(SUM(payment_amount),0)
                    FROM agent_payments   
                    WHERE agent_id = transactions.agent_id) AS TotalAgentPayment,
                    (TRUNCATE(SUM(total_payment) - (SUM(charge_amount)/2),2)-(SELECT IFNULL(SUM(payment_amount),0)
                    FROM agent_payments   
                    WHERE agent_id = transactions.agent_id)) AS TotalAgentBalance'))
            ->groupBy('agents.id')
            ->get();

        // return response()->success(compact('transaction'));
        return json_encode($transaction);
    }
    
    public function searchgeneral(Request $request){


        $wheres = $request->where;
      
        

        $transaction = Transaction
            ::from('transactions AS t')
            ->join('agents AS a',DB::raw('a.id'),'=',DB::raw('t.agent_id'))
            ->join('senders AS s','s.id','=','t.sender_id')
            ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
            ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
            ->join('mode_centers AS mc','mc.id','=','bsc.mode_center_id')
            ->join('service_modes AS sm','sm.id','=','mc.service_mode_id')
            ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
            ->join('transaction_statuses AS ts','ts.id','=','t.transaction_status_id')
            ->join('users as u','u.id','=','t.user_id')
            ->leftJoin('users as ur','ur.id','=','t.approved_by')
            ->leftJoin('users as us','us.id','=','t.completed_by')

            /*->where(function($query) use ($alias,$where,$equal) {
                if ($alias && $where && $equal) {
                    $query->where($alias . '.' . $where, 'like', "$equal%");
                }
            })*/
            ->where(function($query) use ($wheres){
                if($wheres){
                  foreach ($wheres as $where => $equal) {
                      $query->where($equal['choice']['alias'].'.'.$equal['choice']['where'],'like',"$equal[val]%");
                  }

                }   
 
            })
            ->where(function ($query) use ($request){
                $query->whereRaw('t.created_at between ? AND ?',[$request->from,Carbon::parse($request->to)->addDay()]);
            })
            //For security purpose
            ->where(function ($query){
                if (Auth::user()->roles[0]->role =='DirectClient'){
                    $query->where('t.sender_id',Auth::user()->sender->id);
                }
                if (Auth::user()->roles[0]->role =='Agent'){
                    $query->where('t.agent_id',Auth::user()->agent[0]->id);
                }
            })
            ->select('t.*','bsc.*','sm.*','sc.*','ts.status','u.*',
                't.id AS t_id',
                's.id AS sender_id',
                's.fname AS s_fname',
                's.mname AS s_mname',
                's.lname AS s_lname',
                's.gender AS s_gender',
                's.bday AS s_bday',
                's.place_of_birth AS s_place_of_birth',
                's.contact_no AS s_contact_no',
                's.citizenship AS s_citizenship',
                's.residency AS s_residency',
                's.country AS s_country',
                's.address AS s_address',
                's.address_ph AS s_address_ph',
                's.suburb AS s_suburb',
                's.state AS s_state',
                's.post_code AS s_post_code',
                's.occupation AS s_occupation',
                's.source_of_inc AS s_source_of_inc',
                's.email as s_email',

                'b.fname AS b_fname',
                'b.mname AS b_mname',
                'b.lname AS b_lname',
                'b.gender AS b_gender',
                'b.address AS b_address',
                'b.contact_no AS b_contact_no',
                'b.civil_status AS b_civi_status',
                'b.relationship AS b_relationship',
                'sc.id AS SC_id',
                'sm.id AS SM_id',

                    /*athan*/
                't.created_at AS t_cdate',
                'a.business_name AS a_business_name',
                'a.agent_code AS a_agent_code',

                /*dean*/
                'u.fname AS u_fname',
                'u.mname AS u_mname',
                'u.lname AS u_lname',
                'ur.fname AS ur_fname',
                'ur.mname AS ur_mname',
                'ur.lname AS ur_lname',
                'us.fname AS us_fname',
                'us.mname AS us_mname',
                'us.lname AS us_lname'


            )
                    ->get();      

            //return response()->success(compact('tkeyword','bkeyword'));
        return response()->success(compact('transaction'));

             }

    public function findBank(Request $request){
            $keyword = $request->keyword;
            $btransaction = DB::table('transactions')
                        ->where('bank_reference_no','like', "$keyword%")
                        //For security purpose
                        ->where(function ($query){
                            if (Auth::user()->roles[0]->role =='DirectClient'){
                                $query->where('transactions.sender_id',Auth::user()->sender->id);
                            }
                        })
                        ->take(10)->get();
        return response()->success(compact('btransaction'));        
                      
   
    }

    public function search(Request $request){
        $keyword = $request->keyword;
        $transaction = DB::table('transactions')
                        ->where('tracking_no','like', "$keyword%")
                        ->where(function ($query){
                            if (Auth::user()->roles[0]->role =='DirectClient'){
                                $query->where('transactions.sender_id',Auth::user()->sender->id);
                            }
                        })
                        ->take(10)->get();
        return response()->success(compact('transaction'));
    }

    public function volumePerMonth(){
        $volume = Transaction::volumePerMonth();
        return response()->success(compact('volume'));

    }
    public function volumePerDay(){
        $volume =  Transaction::volumePerDay();
        return response()->success(compact('volume'));

    }

    public function countPerServiceMode(){
        $count = Transaction::countPerServiceMode();
        // return response()->success(compact('count'));
        return json_encode($count);

    }

    public function countPerAgent(){
        $count =  Transaction::countPerAgent();
        // return response()->success(compact('count'));
        return json_encode($count);

    }

    public function mluillier(Request $request){
       //$transactions = Tra
    }

    public function findByBanks($status,$from,$to){
        $transactions = Transaction::from('transactions AS t')
            ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
            ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
            ->join('mode_centers AS mc','mc.id','=','bsc.mode_center_id')
            ->join('service_modes AS sm','sm.id','=','mc.service_mode_id')
            ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
            ->join('transaction_statuses AS ts','ts.id','=','t.transaction_status_id')
            //kim
            ->join('senders AS s','s.id','=','t.sender_id')
            ->join('users as u','u.id','=','t.user_id')
            ->leftJoin('users as ur','ur.id','=','t.approved_by')
            ->leftJoin('users as us','us.id','=','t.completed_by')
            ->where('sm.flag',1)
            //->whereRaw('t.created_at between ? AND ?',[$from,Carbon::parse($to)->addDay()])

            //change created_at to approved_at as per requested by ma'am mermory for bank to bank report 11/15/2017
            ->whereRaw('t.approved_at between ? AND ?',[$from,Carbon::parse($to)->addDay()])
            //->whereNotIn('sc.id',[84,95,96,97,12])
            ->where('t.transaction_status_id',$status)
            //For security purpose
            ->where(function ($query){
                if (Auth::user()->roles[0]->role =='DirectClient'){
                    $query->where('t.sender_id',Auth::user()->sender->id);
                }
            })
            ->select('t.receive_amount','t.total_payment','t.charge_amount','t.exchange_rate','t.approved_at','t.tracking_no','t.send_amount','bsc.account_no','sc.service_center','b.fname','b.mname','b.lname','sm.flag','u.fname AS u_fname',
                'u.mname AS u_mname',
                'u.lname AS u_lname',
                'ur.fname AS ur_fname',
                'ur.mname AS ur_mname',
                'ur.lname AS ur_lname',
                'us.fname AS us_fname',
                'us.mname AS us_mname',
                'us.lname AS us_lname',
                's.fname AS s_fname',
                's.mname AS s_mname',
                's.lname AS s_lname')

            ->get();
            return response()->success(compact('transactions'));
    }

    public function getSenderBeneficiaries($last_name,Request $request){
        $agent = $request->input('agent');
        $senders = Sender::
            join('agent_senders','agent_senders.sender_id','=','senders.id')
            ->where('lname','like',"%$last_name%")
            ->where('agent_senders.agent_id',$agent)
            ->select('senders.*','agent_senders.agent_id as agent_id')
            ->get();
        $data = [];
        if(count($senders)){
            for($i=0;$i<= count($senders) -1;$i++){
                $senders[$i]['beneficiaries']= Transaction::from('transactions AS t')
                ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
                ->join('senders AS s','s.id','=','t.sender_id')
                ->join('agents AS a','a.id','=','t.agent_id')
                ->select(
                'b.id AS b_id',
                'b.fname AS b_fname',
                'b.mname AS b_mname',
                'b.lname AS b_lname',
                'b.address AS baddress',
                DB::raw('COUNT(*) as count'))
                ->where('s.id',$senders[$i]->id)
                ->where('a.id',$agent)
                ->groupBy('b.id')
                ->get();
            }
        }

        return response()->success(compact('senders'));
        
    }

    public function getSenderBeneficiaryTransactions($beneficiary_id,Request $request){
        $sender_id = $request->input('sender');
        $agent = $request->input('agent');
        $sender = Sender::findOrfail($sender_id);
        $beneficiary = Beneficiary::findOrfail($beneficiary_id);

        $transactions = Transaction::from('transactions AS t')
        ->join('agents AS a',DB::raw('a.id'),'=',DB::raw('t.agent_id'))
        ->join('senders AS s','s.id','=','t.sender_id')
        ->join('beneficiaries AS b','b.id','=','t.beneficiary_id')
        ->join('beneficiary_service_centers AS bsc','bsc.id','=','t.beneficiary_service_center_id')
        ->join('mode_centers AS mc','mc.id','=','bsc.mode_center_id')
        ->join('service_modes AS sm','sm.id','=','mc.service_mode_id')
        ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
        ->join('transaction_statuses AS ts','ts.id','=','t.transaction_status_id')
        ->select('t.*','bsc.*','sm.*','sc.*',
            's.fname AS s_fname',
            's.mname AS s_mname',
            's.lname AS s_lname',

            's.gender AS s_gender',
            's.bday AS s_bday',
            's.place_of_birth AS s_place_of_birth',
            's.contact_no AS s_contact_no',
            's.citizenship AS s_citizenship',
            's.residency AS s_residency',
            's.country AS s_country',
            's.address AS s_address',
            's.address_ph AS s_address_ph',
            's.suburb AS s_suburb',
            's.state AS s_state',
            's.post_code AS s_post_code',
            's.occupation AS s_occupation',
            's.source_of_inc AS s_source_of_inc',
            's.email as s_email',

            'b.fname AS b_fname',
            'b.mname AS b_mname',
            'b.lname AS b_lname',
            'b.gender AS b_gender',
            'b.address AS b_address',
            'b.contact_no AS b_contact_no',
            'b.civil_status AS b_civi_status',
            'b.relationship AS b_relationship',
            'a.business_name AS a_business_name',
            'a.agent_code AS a_agent_code',
            't.created_at AS created_at'
            
        )
        ->where('s.id',$sender->id)
        ->where('a.id',$agent)
        ->where('b.id',$beneficiary->id)
        ->where('ts.status','Completed')
        ->get();

        return response()->success(compact('sender','beneficiary','transactions'));
    }

    function getLastTransactionId()
    {
        $max = DB::table('transactions')->select(DB::raw('max(id) as maxId'))->first(['maxId']);
        return response()->success($max);
    }

    public function transactionLogs()
    {
        $logs = Transaction::select('tracking_no', 'created_at')
                    ->where('created_at','like',$this->today.'%')
                    ->orderBy('created_at','desc')
                    ->limit(5)
                    ->get();
        
        return json_encode($logs);
    }

    public function transactionChart()
    {
        $data = Transaction::where('created_at','like',$this->today.'%')
                        ->select(DB::raw('SUM(transaction_status_id=1) AS verification, 
                        SUM(transaction_status_id=2) AS approved,
                        SUM(transaction_status_id=3) AS completed'))->get();
        
                        return json_encode($data);
    }

    public function getListOfTransactions()
    {
        $data = DB::table('transactions as t')
                        ->select(DB::raw('ts.status as status, 
                                        CONCAT(u.fname,", ",u.lname) AS createdBy, 
                                        CONCAT(s.lname,", ",s.fname) AS sender, 
                                        CONCAT(b.lname,", ",b.fname) AS beneficiary, 
                                        s.contact_no as contact, t.*, (t.charge_amount+t.send_amount) AS total'))
                        ->leftjoin('users as u', 'u.id','=','t.user_id')
                        ->leftjoin('senders as s', 's.id','=','t.sender_id')
                        ->leftjoin('transaction_statuses AS ts', 'ts.id', '=', 't.transaction_status_id')
                        ->leftjoin('beneficiaries AS b', 'b.id','=','t.beneficiary_id')
                        ->paginate(15);

        return json_encode($data);
    }
}
