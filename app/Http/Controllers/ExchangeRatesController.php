<?php

namespace App\Http\Controllers;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
use App\ExchangeRates;

class ExchangeRatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exrates = ExchangeRates::orderBy('created_at', 'desc')->get();
        // $exrates = DB::table('exchange_rates')->orderBy('created_at')->get();
        // return response()->success(compact('exrates'));
        return json_encode($exrates);
        // return $exrates;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
         $exrate = new ExchangeRates;
        foreach ($request->all() as $key => $value) {
            $exrate->$key = $value;
        }
        $exrate->save();
        return response()->success(compact('exrate'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exrate = ExchangeRates::FindOrFail($id);
        //$beneficiaries = $sender->sender_beneficiaries()->get();
        return response()->success(compact('exrate'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getLast(){
        $exrates = ExchangeRates::orderBy('created_at','desc')->first();
        // return response()->success(compact('exrates'));
        return json_encode($exrates);
    }
    public function showAll(){

        $search = isset($_GET['search']) ? $_GET['search'] : "";
        $exrates = ExchangeRates::join('users','users.id','=','exchange_rates.user_id')
                        ->select('users.*','exchange_rates.*',
                        'users.id as u_id',
                        'users.email as u_id',
                        'users.created_at as u_created_at',
                        'users.updated_at as u_updated_at',
                        'exchange_rates.id as er_id',
                        'exchange_rates.created_at as er_created_at',
                        'exchange_rates.updated_at as er_updated_at',
                        'exchange_rates.rate as er_rate'
                        )
                        ->where('fname','like','%'.$search.'%')
                        ->orwhere('lname','like','%'.$search.'%')
                        ->orwhere('rate','like','%'.$search.'%')
                        ->orwhere('exchange_rates.created_at','like','%'.$search.'%')
                        ->orderBy('er_created_at', 'desc')->paginate(10);

        // return response()->success(compact('exrates'));
        return json_encode($exrates);
    }

  
}
