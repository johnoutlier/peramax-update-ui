<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Sender;
use App\Agent;
use App\AgentSender;
use App\User;
use Auth;

class SenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



     public function getLast(){
        $lastSender = Sender::all()->last();
        return response()->success(compact('lastSender'));
    }

    public function index(){
        $search = isset($_GET['search']) ? $_GET['search'] : '';
        $senders = Sender::where('fname','like','%'.$search.'%')
                            ->orWhere('mname','like','%'.$search.'%')
                            ->orWhere('lname','like','%'.$search.'%')
                            ->orWhere('email','like','%'.$search.'%')
                            ->orWhere('country','like','%'.$search.'%')
                            ->orWhere('citizenship','like','%'.$search.'%')
                            ->orWhere('suburb','like','%'.$search.'%')
                            ->orWhere('state','like','%'.$search.'%')
                            ->orWhere('occupation','like','%'.$search.'%')
                            ->paginate(10);
        return json_encode($senders);
        // return response()->success(compact('senders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'fname' => 'required | min:1| max:35 | regex:/^[a-zA-Z]*$/',
            'lname' => 'required | min:1| max:35 | regex:/^[a-zA-Z]*$/',
            'contact_no' => 'required | max:15 | regex:/^[0-9]*$/',
            'address' => 'required | min:5',
            'source_of_inc' => 'required | min:3',
            'bday' => 'required',
            'occupation' => 'required | min:3',
            'citizenship' => 'required',
            'residency' => 'required',
            'address_ph' => 'required | min:10 | max:255',

        ]);
        $agent = Agent::findOrFail($request->agent_id);
        $sender = new Sender;
        foreach ($request->except('agent_id') as $key => $value) {
            $sender->$key = $value;
        }
        $sender->save();
        $agent->agent_senders()->attach($sender->id);
        return response()->success(compact('sender'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $payload = User::getPayload();
        $role = $role = $payload['role']['role'];
        $agent = $payload['agent']['id'];
        $user = $payload['user']['id'];
        switch ($role){
            case 'Agent':
                $sender = $sender = Sender::Join('agent_senders','agent_senders.sender_id','=','senders.id')
                    ->where('agent_senders.agent_id','=',$agent)
                    ->where('senders.id','=',$id)
                    ->select('senders.*')
                    ->firstOrFail();
                break;
            case 'DirectClient':
                /*$sender = Sender::Join('agent_senders','agent_senders.sender_id','=','senders.id')
                    ->join('user_roles','user_roles.agent_id','=','agent_sender.agent_id')
                    ->where('agent_senders.agent_id','=',$agent)
                    ->where('user_roles.user_id',$user)
                    ->where('senders.id',$id)
                    ->firstOrFail();*/
                $sender = Sender::join('users','users.id','=','senders.user_id')
                ->where('senders.id','=',$id)
                ->select('senders.*'
                    )
                ->firstOrFail();
                break;
            default:
                $sender = Sender::FindOrFail($id);
                break;
        }
        return response()->success(compact('sender'));
    }


    public function takeSender(Request $request)
    {
        $sender = Auth::user()->sender;
        return response()->success(compact('sender'));
    }
    

    public function loadAgentSender(Request $request)
    {  

      $sender = Sender::join('agent_senders','agent_senders.sender_id','=','senders.id')
                        ->where('agent_senders.agent_id','=',$request->AgentId)
                        ->select('senders.*')
                        ->paginate(10);//->toArray();
                        
                        //echo json_encode($sender);
                       // var_dump($sender);
        return response()->success(compact('sender'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function addSender(Request $request)
    {
        $sender = new Sender;
        foreach ($request->all() as $key => $value) {
            $sender->$key = $value;
        }
        $sender->save();
        return response()->success(compact('sender'));

    }

    public function addAgentSender(Request $request)
    {
        $agentsender = new AgentSender;
        foreach ($request->all() as $key => $value) {
            $agentsender->$key = $value;
        }
        $agentsender->save();
        return response()->success(compact('agentsender'));

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id = Auth::user()->roles[0]->role == 'DirectClient' ? Auth::user()->sender->id : $id; //For security 
        $sender = Sender::where('id','=',$id)    
            ->update($request->all());//{
        return response()->success(compact('sender'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search($agent,Request $request){
        $keyword = $request->keyword;
        $senders = Sender::from('senders')
                        ->join('agent_senders','agent_senders.sender_id','=','senders.id')
                        ->join('agents','agents.id','=','agent_senders.agent_id')
                        ->where(function ($query) use ($agent){
                                if ($agent>0){
                                    $query->where('agent_senders.agent_id','=',$agent);
                                }
                        })
                        ->where(function ($query) use ($keyword){
                            $query->where('senders.fname','LIKE',"%$keyword%");
                            $query->orWhere('senders.lname','LIKE',"%$keyword%");
                            $query->orWhere(DB::raw('CONCAT_WS(" ",senders.fname,senders.lname)'),'like', "%$keyword%");
                            $query->orWhere(DB::raw('CONCAT_WS(" ",senders.lname,senders.fname)'),'like', "%$keyword%");        
                            
                            //$query->orWhere('senders.lname','like', "$keyword%");
                            //$query->orWhere('senders.mname','like', "$keyword%");
                        })
                        ->select('senders.*','agents.agent_code','agents.business_name')
                        ->take(10)
                        ->get();
        return response()->success(compact('senders'));
        //$test = $request->all();
        //return response()->success(compact('agent','test'));
    }

    public function searchPaginate($agent,Request $request){
        $keyword = $request->keyword;
        $senders = Sender::from('senders')
                        ->join('agent_senders','agent_senders.sender_id','=','senders.id')
                        //->where('agent_senders.agent_id','=',$agent)
                        ->where(function ($query) use ($agent){
                                if ($agent>0){
                                    $query->where('agent_senders.agent_id','=',$agent);
                                }
                        })
                        ->where(function ($query) use ($keyword){
                            $query->where(DB::raw('CONCAT_WS(" ",senders.fname,senders.lname)'),'like', "%$keyword%");
                            //$query->orWhere('senders.lname','like', "$keyword%");
                            //$query->orWhere('senders.mname','like', "$keyword%");
                        })
                        ->select('senders.*')
                        ->paginate(10);
        return response()->success(compact('senders'));
        //$test = $request->all();
        //return response()->success(compact('agent','test'));
    }


    
    public function count(Request $request){
        $volume = Transaction::
            select(DB::raw('DATE_FORMAT(created_at,"%b %Y") as created,COUNT(*) as total'))
            ->from(DB::raw('(SELECT sender_id, MIN(created_at) as created_at FROM transactions GROUP BY sender_id) AS t1 '))
            ->groupBy(DB::raw('DATE_FORMAT(created_at,"%b %Y")'))
            ->orderBy(DB::raw('DATE_FORMAT(created_at,"%Y"), DATE_FORMAT(created_at,"%m")'))
           ->get();
    return response()->success(compact('volume'));
        /*DB::select(DB::raw('SELECT DATE_FORMAT(created_at,"%b %Y") trans_date,COUNT(*) total FROM
                              (SELECT sender_id, MIN(created_at) created_at FROM transactions GROUP BY sender_id) t1
                                GROUP BY DATE_FORMAT(created_at,"%b %Y")
                                ORDER BY DATE_FORMAT(created_at,"%Y"), DATE_FORMAT(created_at,"%m")'));
          //->from(DB::raw('(SELECT sender_id, MIN(created_at) created_at FROM transactions ) t1'))*/
    }

    public function perAgent($agent){
        $senders = Sender::
            join('agent_senders','agent_senders.sender_id','=','senders.id')
            ->where(function ($query) use ($agent){
                $query->where('agent_senders.agent_id','=',$agent);
            })
            ->get();

        return response()->success(compact('senders'));
    }


}
