<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Agent;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $agents = Agent::paginate(10);
       return json_encode($agents);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $agent = new Agent;
        foreach ($request->all() as $key => $value) {
            $agent->$key = $value;
        }
        $agent->save();
        return response()->success(compact('agent'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $agent = Agent::FindOrFail($id);
        //$beneficiaries = $sender->sender_beneficiaries()->get();
        return response()->success(compact('agent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agent = Agent::where('id','=',$id)
            ->update($request->all());//{
        return response()->success(compact('agent'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function findCode($id){
        $agent = Agent::where('id',$id)->select('agent_code')->first();
        return response()->success($agent);
    }


    /*athan*/
     public function search(Request $request){
        $keyword = $request->keyword;
        $agent = DB::table('agents')
                        ->where('business_name','like', "$keyword%")
                        ->orWhere('agent_code','like', "$keyword%")
                        ->take(10)->get();
        return response()->success(compact('agent'));
    }



}
