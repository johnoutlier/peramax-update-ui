<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\ServiceMode;

class ServiceModeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = isset($_GET['page']) ? $_GET['page'] : "";
        $search = isset($_GET['search']) ? $_GET['search'] : "";
        if($page == 'dashboard'){
            $service_modes = ServiceMode::all();
        }
        else{
            $service_modes = ServiceMode::
                                where('service_code','like','%'.$search.'%')
                                ->orwhere('mode_desc','like','%'.$search.'%')
                                ->orwhere('service_charge','like','%'.$search.'%')
                                ->orwhere('max_amount','like','%'.$search.'%')
                                ->orwhere('lock_amount','like','%'.$search.'%')
                                ->paginate(10);

        }
        // return response()->success(compact('service_modes'));
        return json_encode($service_modes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $service_mode = new ServiceMode;
        foreach ($request->all() as $key => $value) {
            $service_mode->$key = $value;
        }
        $service_mode->save();
        return response()->success(compact('service_mode'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service_mode  = ServiceMode::findOrFail($id);
        $service_centers = $service_mode->service_center()->get();
        return response()->success(compact('service_centers'));
        /*$service_mode = ServiceMode
                        ::from('mode_centers AS mc')
                        ->join('service_modes AS sm', 'sm.id', '=', 'mc.service_mode_id')
                        ->join('service_centers AS sc', 'sc.id', '=', 'mc.service_code_id')
                        ->where(sm)
                        ->select('mc.*', 'sm.*', 'sc.*',
                            'mc.id AS mc_id',
                            'sm.service_code AS sm_service_code',
                            'sm.mode_desc AS sm_mode_desc',
                            'sm.service_charge AS sm_service_charge',

                            'sc.service_center AS sm_service_center',
                            'sc.center_desc AS sm_center_desc'
                            )
                        
                        ->first();

                        return response()->sucess(compact('service_mode'));*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $service_mode = ServiceMode::where('id','=',$id)
            ->update($request->all());//{
        return response()->success(compact('service_mode'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

      /*athan*/

       public function searchMode(Request $request){
        $keyword = $request->keyword;
        $service_mode = DB::table('service_modes')
                        ->where('mode_desc','like', "$keyword%")
                        ->orwhere('service_code','like',"$keyword%")
                        ->take(10)->get();
        return response()->success(compact('service_mode'));
    }

}
