<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Pusher\Laravel\PusherManager;
use Illuminate\Support\Facades\App;

class PusherController extends Controller
{
   protected $pusher;

    public function __construct(PusherManager $pusher)
    {
        $this->pusher = $pusher;
    }

    public function push(){
        

        //return $this->pusher->getSettings();

        //LaravelPusher::connection('main')->log('They see me logging…');
        //$this->pusher->trigger('my-channel', 'event', ['message' => 'testing']);
        //$this->pusher->connection('main')->log('They see me logging…');
        //echo json_encode($this->pusher->getSettings());
        //$test =App::make('pusher');
        //$test->trigger('my-channel', 'event', ['message' => 'testing']);
        $test = $this->pusher->trigger('Admin', 'newTransaction', ['message' => 'testing']);
        //$config = $test->getSettings();
        return response()->success(compact('test','config'));
    }
}
