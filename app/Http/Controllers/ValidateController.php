<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\RegisteredUser;
use App\User;
use App\Sender;
use App\SocialProvider;
use Mail;

class ValidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filter = isset($_GET['search']) ? $_GET['search'] : "";       
        $users = RegisteredUser::where('fname','like','%'.$filter.'%')
                        ->orWhere('mname','like','%'.$filter.'%')
                        ->orWhere('lname','like','%'.$filter.'%')
                        ->orWhere('email','like','%'.$filter.'%')
                        ->orWhere('contact_no','like','%'.$filter.'%')
                        ->paginate(10);
       
        return json_encode($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registered_user_find = RegisteredUser::findOrFail($id);
        $registered_user = $registered_user_find->toArray();
        $user_data= array_only($registered_user,['email','password','fname','mname','lname']);
        $sender_data  = array_except($registered_user,['password','id','created_at','updated_at','is_social','provider_id','provider','validated_by','validated_at','temppassword']);
        $user = new User;
        foreach ($user_data as $key =>$value){
            $user->$key = $value;
        }
        $user->save();
        $user->roles()->attach($user->id,['role_id'=>2,'agent_id'=>2]);
        $sender_data['user_id'] = $user->id;
        $sender = new Sender;
        foreach ($sender_data as $key =>$value){
            $sender->$key = $value;
        }
        $sender->save();
        $sender->agent_senders()->attach($sender->id,['agent_id' => 2]);
        if($registered_user['is_social']){
            $user->socialProviders()->create(
                [
                    'provider_id' => $registered_user['provider_id'],
                    'provider' => $registered_user['provider_id']
                ]
            );
        }
        
        $registered_user_find->delete();
        $message = array('lname' => $registered_user['lname'],'temppassword' => $registered_user['is_social'] ? 'Your temporary password: '. $registered_user['temppassword'] : '');
        $body = $this->setBody($message);
        $this->sendMail($user->email,'Registration Confirmation',$body);
        return response()->success(compact('user','sender'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function test(){
        $test = array('lname' => 'test','temppassword' => 'Your temppassword is 1');
        $body = $this->setBody($test);
        return $this->sendMail('markcalimosa@gmail.com','test',$body);
    }
    public function sendMail($to,$subject,$body){
        if ($to){
            Mail::send(array(),array(), function ($message) use ($to,$subject,$body) {
                $message->to($to, 'Peramax Online')->subject($subject)
                    ->setBody($body,'text/html');
            });
        }

    }

    public function setBody($user){
        $body =  '<!DOCTYPE html>'
            .'<html>'
            .'<head>'
            . '<title>Sender</title>'
            .'</head>'
            .'<body style="font-size: 1.2emfont-family: "Sans Serif">'

            .'<br>'
            .'<div>'
            . '<ul style="list-style: none">'
            .'<li >Dear Mr/Ms '. $user['lname'] .',</li>'
            .'</ul>'
            .'<p style="margin-left: 10%">Thank You for trusting Peramax Money Remittance Service.</p>'
            .'<p style="margin-left: 3%">We would like to inform you that your registration was verified and approved. You can now start using our services.</p>'


            .'</div>'
            .'</br>'
            .'<div>'
            .'</br>'
            .'<ul style="list-style: none">'
            .'<li><p>Click <a href="http://peramax.ftr-dev.com/#/login"> here </a>  to redirect to our site or feel free to visit us any time at <a href="http://peramax.ftr-dev.com"> www.peramax.ftr-dev.com </a><p></li>'
            .'<li>'. $user['temppassword'] . '</li>'
            .'<li></li>'
            .'<li>Thanks,</li>'
            .'<li>Peramax Money Remittance Service</li>'
            .'</ul>'
            .'</div>'

            .'</body>'
            .'</html>';
        return $body;
    }
}
