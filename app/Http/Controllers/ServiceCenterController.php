<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\ServiceCenter;
use App\ModeCenter;
use App\ServiceMode;


class ServiceCenterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = isset($_GET['search']) ? $_GET['search'] : "";

            $serviceCenters = serviceCenter::where('service_center', 'like','%'.$search.'%')
                                            ->orwhere('center_desc', 'like','%'.$search.'%')
                                            ->paginate(10);
        return json_encode($serviceCenters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $service_center = new ServiceCenter;
        foreach ($request->all() as $key => $value) {
            $service_center->$key = $value;
        }
        $service_center->save();
        return response()->success(compact('service_center'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*athan*/
          $serviceCenter = serviceCenter::FindOrFail($id);
        //$beneficiaries = $sender->sender_beneficiaries()->get();
        return response()->success(compact('serviceCenter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $service_center = ServiceCenter::where('id','=',$id)->firstOrFail();
        foreach ($request->except('token') as $key => $value){
            $service_center->$key = $value;
        }
        $service_center->save();

        return response()->success(compact('service_center'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public  function flag(Request $request){
        $service_centers = ServiceCenter::where('btb_flag',$request->btbFlag)
                            ->where('dtd_flag',$request->dtdFlag)->get();

        return response()->success(compact('service_centers')); 
    }
    // removing Service Center
    // check all affected table for validation
    // --dean-- 08-23-2016
    public function removeServiceCenter(Request $request)
      {
       $checkServiceCenter = DB::table("transactions AS trans")
            ->join('beneficiary_service_centers AS bcs','bcs.id','=','trans.beneficiary_service_center_id')
           ->join('mode_centers AS mc','mc.id','=','bcs.mode_center_id')
            ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
            ->where('mc.service_center_id','=',$request->servicecenter)
            ->count();

         if($checkServiceCenter > 0){
            return 'Cannot remove Service Center. Record in Transaction';
            }else{
           /*
            $getModeCenter = ServiceCenter::where('id',$request->servicecenter)
                ->pluck('id');*/

            $removeServiceCenters = ServiceCenter::where('id',$request->servicecenter)
            ->delete();
            return response()->success(compact('removeServiceCenters'));
        }
    }

    //
    //remove service center from service mode
    //check service center id from transaction before deleting
    //by rgb
    //20160725
    //
    
    public function removeCentertoMode(Request $request){
        $checkServiceCenter = DB::table("transactions AS trans")
            ->join('beneficiary_service_centers AS bcs','bcs.id','=','trans.beneficiary_service_center_id')
            ->join('mode_centers AS mc','mc.id','=','bcs.mode_center_id')
            ->where('mc.service_center_id','=',$request->service_center_id)
            ->where('mc.service_mode_id','=',$request->service_mode_id)
            ->count();
        if($checkServiceCenter > 0){
            return 'Cannot remove Service Center. Record in Transaction';
        }else{
            $getModeCenter = DB::table("mode_centers AS mc")
                ->where('mc.service_center_id','=',$request->service_center_id)
                ->where('mc.service_mode_id','=',$request->service_mode_id)
                ->pluck('id');
            $getModeCenter;
            $removeModeCenter = DB::table("mode_centers")
                ->where('id','=',$getModeCenter)
                ->delete();
            return response()->success(compact('removeModeCenter'));
        }
    }


    public function removeMode(Request $request){
            $modeId=$request->modeId;
           $checkServiceCenter = DB::table("transactions AS trans")
            ->join('beneficiary_service_centers AS bcs','bcs.id','=','trans.beneficiary_service_center_id')
             ->join('mode_centers AS mc','mc.id','=','bcs.mode_center_id')
            ->join('service_centers AS sc','sc.id','=','mc.service_center_id')
            ->where('mc.service_mode_id','=',$modeId)
            ->count();
         if($checkServiceCenter > 0){
            return 'Cannot remove Service Center. Record in Transaction';
            }else{     
            $removeModeService = ModeCenter::where('service_mode_id',$modeId)
            ->delete();      
            $removeServiceMode = ServiceMode::where('id',$modeId)
            ->delete();
            return response()->success(compact('removeServiceCenters'));
        }
    }
    /*athan*/

    public function search(Request $request){
        $keyword = $request ->keyword;
        $serviceCenter = DB::table("service_centers")
                            ->where('service_center', 'like', "$keyword%")
                            ->orwhere('center_desc','like',"$keyword%")
                            ->take(10)->get();
        return response()->success(compact('serviceCenter'));
    }
}
