/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('sidepanel', require('./components/sidepanel.vue').default);
Vue.component('transaction', require('./components/TransactionComponent.vue').default);
Vue.component('servicecharge', require('./components/ServiceChargeComponent.vue').default);
Vue.component('servicecharge', require('./components/ServiceChargeComponent.vue').default);
Vue.component('user-validate', require('./components/UsersValidateComponent.vue').default);
Vue.component('exchangerate', require('./components/ExchangerateComponent.vue').default);
Vue.component('agentpayables', require('./components/AgentPayablesComponent.vue').default);
Vue.component('totaldailytransaction', require('./components/TotalDailyTransactionComponent.vue').default);
Vue.component('logs', require('./components/LogsTransactionsComponent.vue').default);
Vue.component('create-transaction', require('./components/CreateTransactionComponent.vue').default);
Vue.component('list-transaction', require('./components/TransactionListComponent.vue').default);
Vue.component('branch-payment', require('./components/branchPaymentComponent.vue').default);
Vue.component('hold-branch', require('./components/holdBranchComponent.vue').default);
Vue.component('general-report', require('./components/GeneralReportComponent.vue').default);
Vue.component('austrac', require('./components/AustracReportComponent.vue').default);
Vue.component('counts', require('./components/CountsComponent.vue').default);
Vue.component('certificate', require('./components/CertificateComponent.vue').default);
Vue.component('banktobank', require('./components/BanktoBankComponent.vue').default);
Vue.component('users-list', require('./components/usersListComponent.vue').default);
Vue.component('senders-list', require('./components/SendersComponent.vue').default);
Vue.component('branches', require('./components/BranchesComponent.vue').default);
Vue.component('mode-list', require('./components/ServiceModesComponent.vue').default);
Vue.component('service-center', require('./components/ServiceCenterComponent.vue').default);
Vue.component('exchangerate-all', require('./components/ExchangeRateAllComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

