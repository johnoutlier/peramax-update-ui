@extends('layouts.app')
@section('content')
<div class='content'>
    <div id="app">
    <sidepanel></sidepanel>
    </div>
</div>

<div class="container">
    <div id='app'>
        <users-list></users-list>
    </div>
</div>
@endsection