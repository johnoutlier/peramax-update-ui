@extends('layouts.app')
@section('content')
<div class='content'>
    <div id="app">
    <sidepanel></sidepanel>
    </div>
</div>

<div class="container">
    <div id='app'>
        <user-validate></user-validate>
    </div>
</div>
@endsection