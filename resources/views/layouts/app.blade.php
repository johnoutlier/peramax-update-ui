<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/carvue/dist/build.min.js" ></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/carvue/dist/carvue.min.css" rel="stylesheet">



    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sidepanel-style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/peramax.css') }}" rel="stylesheet">

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <i class="fa fa-navicon" style="color:#F2CA50;font-size: 30px"></i>



            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a  href="#" style="margin : 30px;">
                    <img src="{{url('/')}}/storage/img/PeraMax Logo.svg" alt="" height="30px">
                </a>
            </li>



        </ul>
        <ul class="navbar-nav ">
            <li class="nav-item">
            <i class="fa fa-bell" style=" font-size: 20px;
            text-shadow: -2px 0 #F2CA50, 0 2px #F2CA50, 2px 0 #F2CA50, 0 -2px #F2CA50;

            ">

        </i>


    </li>

</ul>


</nav>

<main class="py-4">
    @yield('content')
</main>
</div>
</body>
</html>
