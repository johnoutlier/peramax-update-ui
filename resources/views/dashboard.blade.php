@extends('layouts.app')


@section('content')

<div class='content'>
    <div id="app">
        <sidepanel></sidepanel>
    </div>

    <div id ="dashboard-section">
        <div class="container">
            <div class="row">
                <div class="col-4-1">
                    <div class="card">
                        <div class="card-header">
                            Transaction
                        </div>
                        <div id="app">
                            <transaction></transaction>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                        Total Daily Transaction
                        </div>
                        <div id="app">
                        <totaldailytransaction></totaldailytransaction>
                        
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            Agent Payables
                        </div>
                        <div id="app">
                        <agentpayables></agentpayables>
                        </div>
                    </div>
                </div>

                <div class="col-4-2">                    
                    <div class="card">
                        <div class="card-header">
                          Exchange Rate
                        </div>
                        <div id="app">
                        <exchangerate></exchangerate>
                       
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                           Logs of Transactions
                        </div>
                        <div id="app">
                            <logs></logs>
                        </div>
                    </div>
                    
                </div>

                <div class="col-4-3">
                    <div class="card">
                        <div class="card-header">
                            Service Charge
                        </div>
                        <div id="app">
                            <servicecharge></servicecharge>
                        </div>
                    </div>
                </div>

            
            </div>

    
            </div>
        </div>
</div>


@endsection
