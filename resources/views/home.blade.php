@extends('layouts.app')


@section('content')

<div class='content'>
    <div id="app">
    <sidepanel></sidepanel>
    </div>

</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div id="app" class="card-header">Dashboard</div>

               <example-component>
               </example-component>

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
